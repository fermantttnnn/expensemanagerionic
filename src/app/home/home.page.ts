import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { Expense, ExpenseService } from './expense.service';
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss']
})
export class HomePage implements OnInit {
  expensePurpose: string;
  expenseAmount: number;
  category: 1 | -1;
  expenses: Expense[];
  currency;
  expenseDate: Date = new Date();
  currentMonth = new Date().getMonth();
  currentYear = new Date().getFullYear();
  segment: string = 'all';
  constructor(private expenseService: ExpenseService) {}

  ngOnInit() {
    this.expenses = this.expenseService.getExpenses();
    this.currency = this.expenseService.currency;
  }
  getData(){
    switch (this.segment) {
      case 'all':
        this.expenses = this.expenseService.getExpenses();
        break;
      case 'month':
          this.expenses = this.expenseService
          .getExpenses()
          .filter(x => new Date(x.date).getMonth() === this.currentMonth && new Date(x.date).getFullYear() === this.currentYear);
          break;
      case 'year':
          this.expenses = this.expenseService
          .getExpenses().filter(x => new Date(x.date).getFullYear() === this.currentYear);
          break;
      default:
        break;
    }
  }
  segmentChanged(ev: any) {
    this.segment=ev.detail.value;
    this.getData();
  }
  add() {
    this.expenseService.addExpense({
      purpose: this.expensePurpose,
      amount: this.expenseAmount,
      category: this.category,
      date: this.expenseDate
    });
    this.getData();
  }
  clear() {
    this.expenseAmount = null;
    this.expensePurpose = null;
  }

  getDate(item: Date) {
    if (item) {
      return (
        item.toString().substring(0, 10) +
        ' ' +
        item.toString().substring(11, 16)
      );
    }
  }

  delete(item: Expense) {
    this.expenseService.deleteExpense(item);
    this.getData();
  }

  sum() {
    return this.expenseService.getSum(this.expenses);
  }
  save() {
    this.expenseService.saveExpenses();
  }
}
