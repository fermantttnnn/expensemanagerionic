import { Injectable } from '@angular/core';
import { AlertController, PopoverController, ToastController } from '@ionic/angular';
declare var require: any;
@Injectable({
  providedIn: 'root'
})
export class ExpenseService {

  private expenses: Expense[] = [];
  currency = 'USD';
  constructor(
    private alertCtrl: AlertController,
    private toast: ToastController
  ) {
    if(localStorage.expenses){
      this.expenses = JSON.parse(localStorage.getItem("expenses")) as Expense[];
    }
   }

  getExpenses(){
    return [...this.expenses];
  }
  addExpense(expense: Expense){
    if (expense.purpose && expense.amount  && expense.category && expense.date){
      this.expenses.push(expense);
    } else{
      this.alertCtrl.create({
        header: 'Empty',
        message: 'Please fill all inputs!',
        animated: true,
        buttons: [
          {
            text: 'Okay',
            role: 'okay'
          }
        ]
      }).then(alert => {alert.present(); });
    }
  }
  deleteExpense(expense: Expense){
    const index = this.expenses.indexOf(expense);
    this.expenses.splice(index, 1);
  }
  getSum(expenses:Expense[]) {
    let sum = 0;
    expenses.forEach(x => {
      sum += x.amount * x.category;
    });
    return sum;
  }
  saveExpenses(){
    localStorage.setItem("expenses",JSON.stringify(this.expenses));
    this.toast.create({
      message:"Saved successfully!",
      animated:true,
      closeButtonText:'Got it!',
      duration:1000
    }).then(p=>
      {
        p.present();
      });
  }
}

export class Expense {
  purpose: string;
  amount: number;
  category: 1 | -1;
  date: Date;
}