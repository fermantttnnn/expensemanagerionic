import { Component } from '@angular/core';

import { Platform, AlertController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Plugins } from '@capacitor/core';
import { ExpenseService } from './home/expense.service';
const App = Plugins.App;
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private alertCtrl: AlertController,
    private expService:ExpenseService
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      App.addListener('backButton',(data)=>{
        this.alertCtrl.create({
          header:'Exit',
          message:'Are you sure want to exit?',
          buttons:[
            {
              text:'Cancel',
              role:'cancel'
            },
            {
              text:'Save and exit',
              handler:()=>{
                this.expService.saveExpenses();
                App.exitApp();
              }
            },
          ]
        }).then(a=>a.present());
      });
    });
    
  }
}
